---
MorpheusModelID: M9148

title: "Hepatocellular Carcinoma"

authors: [S. Höhme, F. Bertaux, W. Weens, B. Grasl-Kraupp, J. G. Hengstler, D. Drasdo]
contributors: [L. Brusch, D. Jahn]
submitters: [L. Brusch, D. Jahn]
curators: [L. Brusch, D. Jahn]

# Under review
hidden: false
private: false

# Reference details
publication:
  doi: "10.1007/s11538-017-0375-1"
  title: "Model Prediction and Validation of an Order Mechanism Controlling the Spatiotemporal Phenotype of Early Hepatocellular Carcinoma"
  journal: "Bull. Math. Biol."
  volume: 80
  #issue:
  page: "1134–1171"
  year: 2018
  original_model: false
  preprint: false

tags:
- 2D
- Agent-based Simulation
- Aspect Ratio
- Asymmetric Shape
- Asymmetry
- Cancer Phenotype
- Carcinogen
- Carcinoma
- Cell Cluster
- Cluster Axis
- CPM
- Cross Section
- Elongation
- Genotoxic Carcinogen
- Genotoxicity
- Growth Kinetics
- GST-P
- Hepatic Lobule
- Hepatocellular Carcinoma
- Hepatocellular Tumor
- Hepatocyte
- Hepatocyte–Sinusoid Alignment
- HSA
- Liver
- Liver Cancer
- Liver Lobule
- Liver Regeneration
- Lobule
- Mammal
- Mammalian Liver 
- Mouse
- Multicellular Model
- N-nitrosomorpholine
- NMOR
- NNM
- Oriented Cell Division
- Population
- Rat
- Rat Liver
- Sinusoid
- Spatiotemporal Phenotype
- StopTime
- Tumor
- Tumor Clone
- Tumor Growth
- Tumor Growth Kinetics

#categories:
#- DOI:10.1007/s11538-017-0375-1
#- DOI:10.1073/pnas.0909374107
---
> Hepatocyte–sinusoid alignment (HSA) causes asymmetric shapes in early hepatocellular tumors.

## Introduction

The mammalian liver is subdivided into lobes and further into thousands of hexagonal columns, called lobules, which act in parallel. Blood enters a lobule at the periphery, flows through a network of sinusoids and leaves at the center. The sinusoids may also prepattern the growth of cell clones. This model studies the influence of hepatocyte–sinusoid alignment (HSA), i.e. the orientation of the division plane such that daughter hepatocytes align with nearby sinusoids, on the shape of tumor nodules in liver lobules ([Fig. 1](#fig-1)).

<div id="fig-1">
![](hoehme-2018_fig-5_rat-liver.jpg "<strong>Figure 1. </strong> Elongated shapes of GST-P positive cell clusters in rat livers, early in (a) and late in (b), after administration of a genotoxic carcinogen ([CC BY 4.0](https://creativecommons.org/licenses/by/4.0/): [**Höhme _et al._**](#reference), [Fig. 5](https://link.springer.com/article/10.1007/s11538-017-0375-1/figures/5)). The data show a decreasing aspect ratio of long over short cluster axes from early to late stages.")
</div>

## Description

As the original model for early hepatocellular carcinoma ([Fig. 2](#fig-2)) had been constructed by extending a model for liver regeneration by <a href="/model/m9147/#reference" title="<i class='fas fa-dna pr-1'></i>Morpheus Model ID: <code class='model-id-tooltip'>M9147</code>">Höhme et _al._, 2010</a>, we here equally extended the corresponding MorpheusML model (<a href="/model/m9147/" title="<i class='fas fa-dna pr-1'></i>Morpheus Model ID: <code class='model-id-tooltip'>M9147</code>">M9147</a>). 

While the original model considered a three-dimensional liver lobule, the MorpheusML model currently (only) represents the two-dimensional cross section with correspondingly lower cell numbers. Also the total simulation duration is reduced to 7 days compared to 10 days in the original model. Starting with only one tumor cell in the liver lobule embedded in 675 hepatocytes (instead of around 4000 hepatocytes in the original model), the tumor gains 400 tumor cells by the end of the simulated time.

All other parameters and observables are comparable. At time $`0\ \mathrm{d}`$ (after the initialization period finished), a single hepatocyte switches to a high proliferation mode and its daughter cells inherit this parameter set, forming a tumor clone. To compare tumor shapes within a single simulation run, two tumors are initialized, one placed in the lower left with HSA and another in the top right without HSA. The aspect ratio of long and short shape axes is approximately measured and visualized by the model itself.

<div id="fig-2">
![](hoehme-2018_fig-3_tumor-growth-scenarios.jpg "<strong>Figure 2. </strong> Scenarios of tumor growth in a single liver lobule in <strong>(a, c)</strong> the presence of HSA, and <strong>(b)</strong> in the absence of HSA. In <strong>(c)</strong>, the tangential friction impeding hepatocyte movement perpendicular to the formed columns along the sinusoids is elevated compared to (a). ([CC BY 4.0](https://creativecommons.org/licenses/by/4.0/): [**Höhme _et al._**](#reference), [Fig. 3](https://link.springer.com/article/10.1007/s11538-017-0375-1/figures/3))")
</div>

## Results

The Morpheus model qualitatively reproduces the original model (see [Video 1](#vid-1)). HSA (light purple cells) causes early asymmetry of tumor cell clusters that decreases later. The time course of the aspect ratio was quantified in [Höhme _et al._](#reference) and is shown in [Fig. 3](#fig-3). Likewise, this time course has been quantified for the Morpheus simulations and plotted in [Fig. 4](#fig-4) (noisy curves from a single realization) and compared to the upper and lower limits from the original study (straight lines).

<figure id="vid-1">
  ![](M9148_hcc_vid-1_simulation.mp4)
  <figcaption>
    <strong>Video 1.</strong> <strong>Morpheus simulation.</strong> Two clones, one with HSA (light purple cells), the other without HSA (green cells), grow in a liver lobule cross section.
  </figcaption>
</figure>

<div id="fig-3">
![](hoehme-2018_fig-4_tumor-axis-ratios.jpg "<strong>Figure 3. Simulations by [**Höhme _et al._**](#reference)</strong> Comparison of the ratio of the longest versus shortest tumor axis. <strong>Black:</strong> With HSA. <strong>Red:</strong> No HSA. ([CC BY 4.0](https://creativecommons.org/licenses/by/4.0/): [**Höhme _et al._**](#reference), [Fig. 4](https://link.springer.com/article/10.1007/s11538-017-0375-1/figures/4))")
</div>

<div id="fig-4">
![](M9148_hcc_fig-4_aspect-ratio@2x.jpg "<strong>Figure 4. Morpheus simulation results.</strong> <strong>Left:</strong> Tumors in the absence of HSA, in which tumor cells grew and divided in random directions, show small aspect ratios that match the range of the original data (straight lines for upper and lower limits). <strong>Right:</strong> Tumors in which cells obey HSA show significant elongation and high aspect ratio that decreases with time matching the original data (straight lines for upper and lower limits). The noisy curve represents one simulation run.")
</div>

Due to the increased competition for space in the two-dimensional cross section compared to the three-dimensional space in the published model, tumors grow slightly slower in the reproduced model ([Fig. 6](#fig-6)) than in the original model ([Fig. 5](#fig-5)). Nonetheless, the exponential form of the growth kinetics is reproduced as well as the slightly faster growth of clones with HSA.

<div id="fig-5">
![](hoehme-2018_fig-14_tumor-growth-kinetics.jpg "<strong>Figure 5. Simulation results by [**Höhme _et al._**](#reference)</strong> Growth kinetics of tumors in case of HSA (black) and no HSA (red) ([CC BY 4.0](https://creativecommons.org/licenses/by/4.0/): [**Höhme _et al._**](#reference), [Fig. 14](https://link.springer.com/article/10.1007/s11538-017-0375-1/figures/14))")
</div>

<div id="fig-6">
![](M9148_hcc_fig-6_tumor-growth-kinetics.png "<strong>Figure 6. Morpheus simulation results.</strong> Tumor growth kinetics show exponential growth, with presence of HSA shown in purple and absence of HSA in green.")
</div>
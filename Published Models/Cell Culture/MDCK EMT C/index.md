---
MorpheusModelID: M7123

title: "MDCK EMT C"

authors: [N. Mukhtar, E. N. Cytrynbaum, L. Edelstein-Keshet]
contributors: [E. N. Cytrynbaum, Y. Xiao]

# Under review
#hidden: true
#private: true

# Reference details
publication:
  doi: "10.1016/j.bpj.2022.04.010"
  title: "A multiscale computational model of YAP signaling in epithelial fingering behavior"
  journal: "Biophys. J."
  volume: 121
  issue: 10
  page: 1940-1948
  year: 2022
  original_model: true

categories:
- DOI:10.1016/j.bpj.2022.04.010

tags:
- 2D
- Adherent Slow Cells
- Adhesion
- Antibody
- aspherity
- Cancer Metastasis
- Cell-Cell Adhesion
- CellDivision
- Cell Migration
- Cellular Potts Model
- Cell Shape
- Cell Speed
- CellType
- cell.volume
- Collective Invasion
- CPM
- DirectedMotion
- E-cadherin
- Embryonic Development
- EMT
- Epithelial Fingering
- Epithelial-Mesenchymal Transition
- Epithelial Sheet
- Epithelium
- Finger-like Projection
- GTPase
- Intracellular Signaling
- Mechanochemical Control
- Mesenchyme
- Metastasis
- Motile Loose Cells
- Multiscale Model
- ODE
- Ordinary Differential Equation
- Rac1
- Shape Index
- Sheet Morphology
- Single Cell Model
- SurfaceConstraint
- Topographic Cues
- VolumeConstraint
- Wound Healing
- YAP
- YAP1
- YAP65
- Yes-associated Protein
---
> Multiple models in epithelial-mesenchymal transitions (EMT) 

## Introduction

We model epithelial-mesenchymal transition (EMT) by first assembling an ODE model for intracellular Yes-associated protein (YAP) signalling and then embedding this single cell model within individual cells in a multiscale simulation. This page contains XML files of the models investigated in the paper by [Mukhtar&nbsp;*et&nbsp;al.*&nbsp;(2022)](#reference). 

## Description

### Effect of Adhesion and Cell Speed

[Park et al. (2019)](Park2019) showed experimentally that blocking the adhesive function of E-cadherin with antibodies led to less stable fingers and more breakage of cells and clusters than in control experiments. The following figure shows that simultaneously varying E-cadherin-dependent adhesion (`A2`) and Rac-1-dependent cell speed (`C2`) has significant effect on sheet morphology. Cell sheets expand even at low `C2`, driven by pressure from cell division at the back. Increasing cell speed and lowering adhesion correlate with longer, more fragile fingers, and massive dissemination. Stronger adhesion favors more stable, long sheet protrusions. Interestingly, low cell speed (`C2` = `1`, left column) can be compensated by high adhesion (`A2` = `24`, top left) in enabling invasive growth. As this sequence indicates, a strong adhesive force between cells can help to combine weak individual cell-migratory forces into a single collective group force that pushes a large sheet protrusion outward. The two extremes illustrate the dichotomy between rapid metastasis versus stable collective invasion.

![](C2vsA2.png "Effect of cell-cell E-cadherin-dependent adhesion (`A2`) and Rac1-dependent cell speed (`C2`) on sheet morphology. All images at $`t = 1500\ \text{MCS}`$. Produced with [`C2vsA2_main.xml`](#downloads) (with cell division restricted to cell volume). Color depicts the relative level of E-cadherin in each cell.")

### Effect of Cell Biophysical Properties

Biophysical properties of the cells effect cell morphology. The ‘cell shape index’ $`q_0 = p_0/\sqrt{a_0}`$ was adopted to represent the cell's preferred shape in 2D, and $`q = p/\sqrt{a}`$ represents the cell's actual shape index, where $`p_0`$ is the cell's preferred perimeter, $`a_0`$ is the cell's preferred area, $`p`$ is the cell's actual perimeter and $`a`$ is the cell's actual area. As shown in the following figure, low speed and preferred shape index corresponds to sluggish, solid-like ‘jammed’ behaviour and a relatively flat leading edge. Increasing $`q_0`$ at low speed results in more fluid, fat fingering. Keeping $`q_0`$ low while increasing cell speed accelerates expansion, with minimal fingering. Increasing both $`q_0`$ and the cell speed leads to thinner, faster fingers, with eventual breakage and massive dissemination.  

![](BiPlot.png "Effect of the preferred cell shape index $`q_0`$ and the Rac1-dependent cell speed C2 on sheet morphology. As in the [main text](#reference) [Fig. 6](https://ars.els-cdn.com/content/image/1-s2.0-S0006349522002843-mmc2.pdf), but with cell division allowed only for cells with `cell.volume` > `40`. At lower left, most cells are ‘jammed’ (purple). At upper right, the sheet is fluid, with finer, long, fast-growing fingers. Produced with [`BiPlot.xml`](#downloads). All results are at $`t = 1500\ \text{MCS}`$.")

## Results

![](C2vsA2.mp4)
**Video of the model simulation {{< model_quick_access "media/model/m7123/C2vsA2_main.xml" >}}:** Effect of cell-cell E-cadherin-dependent adhesion (here <span title="Global/Constant[symbol=&quot;A2&quot;][value] = 12">`A2` = `12`</span>) and Rac1-dependent cell speed (here <span title="Global/Constant[symbol=&quot;C2&quot;][value] = 4">`C2` = `4`</span>) on sheet morphology (with cell division restricted to <span title="CellTypes/CellType[name=&quot;dividingcell&quot;]/CellDivision/Condition[value] = &quot;(rand_uni(0,1)<0.006*(exp(-time/300)+0.2)) * (cell.center.x<40) * (cell.volume>40)&quot;">`cell.volume` > `40`</span>). Color depicts the relative level of E-cadherin in each cell (see [Fig. 5](https://ars.els-cdn.com/content/image/1-s2.0-S0006349522002843-mmc2.pdf) in [Mukhtar et al. (2022)](#reference)).

![](qvsC2.mp4)
**Video of the model simulation {{< model_quick_access "media/model/m7123/qvsC2.xml" >}}:** Default simulation on the effect of the preferred **shape index**, <span title="CellTypes/CellType[name=&quot;dividingcell&quot;]/System/Rule[symbol-ref=&quot;q&quot;]/Expression = &quot;cell.surface/sqrt(cell.volume)&quot;">$q_0 = \frac{p_0}{\sqrt{a_0}}$</span> and the Rac1-dependent **cell speed**, `C2`, on sheet morphology. The color map depicts the
individual cells’ shape index $q$. The default simulation has <span title="Global/Constant[symbol=&quot;C2&quot;][value] = 4">`C2` = `4`</span> and $q_0$ resulting from the aspherity attributes <span title="CellTypes/CellType[name=&quot;dividingcell&quot;]/SurfaceConstraint[mode=aspherity][strength] = 1">`strength` =  `1`</span> and <span title="CellTypes/CellType[name=&quot;dividingcell&quot;]/SurfaceConstraint[mode=aspherity][target] = 1">`target` = `1`</span> (see [Fig. 6](https://ars.els-cdn.com/content/image/1-s2.0-S0006349522002843-mmc2.pdf) in [Mukjtar et al. (2022)](#reference)).

[Park2019]: https://www.nature.com/articles/s41467-019-10729-5
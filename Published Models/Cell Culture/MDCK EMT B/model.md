![](model-graph.svg "Model Graph of `NRA-Y-KD_main.xml`")

{{% callout note %}}
For more information on the models **discussed here**,

- {{< model_quick_access "media/model/m7122/NRA-Y-KD_main.xml" >}} (produced SI Fig. S6) and
- {{< model_quick_access "media/model/m7122/NRA-Y-OE.xml" >}} (produced SI Fig. S7),

see the [supplemental information](https://ars.els-cdn.com/content/image/1-s2.0-S0006349522002843-mmc1.pdf) of the [referenced paper](https://ars.els-cdn.com/content/image/1-s2.0-S0006349522002843-mmc2.pdf).
{{% /callout %}}
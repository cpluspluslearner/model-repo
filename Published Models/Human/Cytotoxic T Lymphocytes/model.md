{{% callout note %}}
This model also requires the separate file [`boundary.tiff`](#downloads).
{{% /callout %}}

![](modelgraph.svg "Model Graph")
<?xml version='1.0' encoding='UTF-8'?>
<MorpheusModel version="4">
    <Description>
        <Details></Details>
        <Title>Active transport through cells</Title>
    </Description>
    <Space>
        <Lattice class="square">
            <Neighborhood>
                <Order>1</Order>
            </Neighborhood>
            <Size value="50, 200, 0" symbol="size"/>
            <BoundaryConditions>
                <Condition boundary="x" type="periodic"/>
                <Condition boundary="-x" type="periodic"/>
                <Condition boundary="y" type="periodic"/>
                <Condition boundary="-y" type="periodic"/>
            </BoundaryConditions>
        </Lattice>
        <SpaceSymbol symbol="space"/>
        <MembraneLattice>
            <Resolution value="100" symbol="memsize"/>
            <SpaceSymbol symbol="memspace"/>
            <Annotation>Membrane Lattice is necessary here as a mean to enable Anisotropic secretion towards a specific direction.

Please see the docu for more details on MembraneLattice.</Annotation>
        </MembraneLattice>
    </Space>
    <Time>
        <StartTime value="0"/>
        <StopTime value="10000"/>
        <TimeSymbol symbol="time"/>
    </Time>
    <CellTypes>
        <CellType name="cell" class="biological">
            <VolumeConstraint target="500" strength="1"/>
            <SurfaceConstraint mode="aspherity" target="1" strength="1"/>
            <MembraneProperty name="membrane substrate" tags="membrane" value="1+sin(-memspace.phi)" symbol="m">
                <Diffusion rate="0.0"/>
                <Annotation>MembraneProperty is handling the directed secretion of the substrate, by defining the concentration as a sine function of the membrane lattice. This way, the concentration can be high on one side and zero on the other side.</Annotation>
            </MembraneProperty>
            <Chemotaxis field="c" strength="0.5">
                <Annotation>Chemotaxis away from the secreted molecule, yielding in cell motility through secretion.</Annotation>
            </Chemotaxis>
        </CellType>
    </CellTypes>
    <CPM>
        <Interaction/>
        <ShapeSurface scaling="norm">
            <Neighborhood>
                <Order>3</Order>
            </Neighborhood>
        </ShapeSurface>
        <MonteCarloSampler stepper="edgelist">
            <MCSDuration value="1"/>
            <MetropolisKinetics temperature="1"/>
            <Neighborhood>
                <Order>2</Order>
            </Neighborhood>
        </MonteCarloSampler>
    </CPM>
    <CellPopulations>
        <Population size="0" type="cell">
            <InitCellObjects mode="distance">
                <Arrangement repetitions="1, 1, 1" displacements="1, 1, 1">
                    <Sphere center="25,25,0" radius="10"/>
                </Arrangement>
            </InitCellObjects>
        </Population>
    </CellPopulations>
    <Analysis>
        <Gnuplotter time-step="100">
            <Terminal name="png" size="4000, 4000, 0"/>
            <Plot>
                <Cells value="m">
                    <ColorMap>
                        <Color value="2" color="red"/>
                        <Color value="1" color="yellow"/>
                        <Color value="0.0" color="white"/>
                    </ColorMap>
                </Cells>
            </Plot>
            <Plot>
                <Field min="0" max="160" symbol-ref="c"/>
            </Plot>
        </Gnuplotter>
        <Logger time-step="-1">
            <Input force-node-granularity="true">
                <Symbol symbol-ref="m"/>
            </Input>
            <Output>
                <TextOutput/>
            </Output>
            <Plots>
                <SurfacePlot>
                    <Color-bar>
                        <Symbol symbol-ref="m"/>
                    </Color-bar>
                    <Terminal plot-size="400, 800, 0.0" terminal="png"/>
                </SurfacePlot>
            </Plots>
        </Logger>
        <ModelGraph format="dot" include-tags="#untagged" reduced="false"/>
    </Analysis>
    <Global>
        <Field name="global substrate" tags="global" value="0" symbol="c">
            <Diffusion rate="0.01"/>
            <Annotation>Global Field for substrate. This field is initialized as 0 and will then "take up" the substrate that is secreted by the cell.</Annotation>
        </Field>
        <System name="Secretion System" tags="membrane, global" time-step="1.0" solver="Runge-Kutta [fixed, O(4)]">
            <DiffEqn symbol-ref="c">
                <Expression>m - 0.01*c</Expression>
            </DiffEqn>
        </System>
        <Constant name="membrane substrate" tags="membrane" value="0" symbol="m">
            <Annotation>We need to introduce another constant for the substrate in the Global scope to be able to define a membrane property in the Cell scope. </Annotation>
        </Constant>
    </Global>
</MorpheusModel>

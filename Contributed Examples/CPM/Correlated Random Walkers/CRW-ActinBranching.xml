<MorpheusModel version="4">
    <Description>
        <Title>CRW-ActinBranchingUnlimited Arp2-3</Title>
        <Details>Full title:		Correlated Random Walkers
Authors:		L. Edelstein-Keshet
Contributors:	D. Jahn
Date:		19.02.2021
Software:		Morpheus (open-source). Download from https://morpheus.gitlab.io
Model ID:		https://identifiers.org/morpheus/M2000
Reference:		L. Edelstein-Keshet: Mathematical Models in Cell Biology
Comment:		Particles moving left and right (as well as up) to mimic actin filament barbed ends as in the paper: Grimm HP, Verkhovsky AB, Mogilner A, Meister JJ. Analysis of actin dynamics at the leading edge of crawling cells: implications for the shape of keratocyte lamellipodia. European Biophysics Journal. 2003 Oct 1;32(6):563-77. Initialize with a few close to y = 0, assign them random left or right directions. Asymmetric "cell division" is used to create branches at some constant probability. One daughter goes left, one goes right. This simulation mimics the "unlimited Arp2/3 scenario" where branching is not limited, and where branching takes place only close to the filament barbed ends. The trajectory of the particles represents the actin filaments. The cell x coordinate modulo the size of the x range is also kept, so that the cell trajectories "wrap around" (so we see "the filaments" of ends that wandered off the edge of the domain).</Details>
    </Description>
    <Global>
        <Constant symbol="xc" value="0.0"/>
    </Global>
    <Space>
        <SpaceSymbol symbol="space"/>
        <Lattice class="square">
            <Size symbol="size" value="200, 200, 0"/>
            <BoundaryConditions>
                <Condition type="periodic" boundary="x"/>
                <Condition type="noflux" boundary="y"/>
                <Condition type="noflux" boundary="-y"/>
                <Condition type="periodic" boundary="-x"/>
            </BoundaryConditions>
            <Neighborhood>
                <Order>2</Order>
            </Neighborhood>
        </Lattice>
    </Space>
    <Time>
        <StartTime value="0"/>
        <StopTime value="1400"/>
        <TimeSymbol symbol="time"/>
    </Time>
    <CellTypes>
        <CellType class="medium" name="medium"/>
        <CellType class="biological" name="BarbedEnd">
            <Annotation>The barbed ends are assumed to grow left and right along a "motile cell edge" (not explicitly modeled). The actin filaments are depicted by the trails formed by barbed ends. Those are shown in the analysis, but not modeled explicitly otherwise.</Annotation>
            <VolumeConstraint target="50" strength="1"/>
            <ConnectivityConstraint/>
            <Property symbol="s" name="the direction" value="if(rand_uni(0,1)&lt;0.5,1,-1)"/>
            <Property symbol="nd" name="Number of divisions (branch order) so far" value="0.0"/>
            <Property symbol="xc" name="x coordinate modulo 200" value="0.0"/>
            <DirectedMotion direction="s, 1, 0.0" name="Move right/left (+1, -1) and up" strength="1.0"/>
            <CellDeath name="some death to avoid too many ">
                <Condition>rand_uni(0,1)&lt;0.0002</Condition>
                <Annotation>The "death" rate represents capping of an actin filament that prevents a barbed end from growing further. Those ends are essentially "dead".</Annotation>
            </CellDeath>
            <CellDivision orientation="1.0, 0.0, 0.0" division-plane="oriented" daughterID="daughter" name="Assymetric division">
                <Condition>rand_uni(0,1)&lt;0.002</Condition>
                <Triggers name="Create one daughter going left, one going right">
                    <Rule symbol-ref="s" name="assign different direction to each daughter">
                        <Expression>if(daughter==1,-1,1)</Expression>
                    </Rule>
                    <Rule symbol-ref="nd" name="increase branch order by one">
                        <Expression>nd+1</Expression>
                    </Rule>
                </Triggers>
                <Annotation>The "division" represents an event where the actin filament branches (via the Arp2/3 complex). Here the branching is assumed to take place right at the barber end. Each branching results in one daughter branch going left and a second going right. This is implemented using the asymmetric cell division of Morpheus.</Annotation>
            </CellDivision>
            <System name="x coordinate modulo 200" solver="Dormand-Prince [adaptive, O(5)]">
                <Rule symbol-ref="xc" name="Compute x coordinate modulo size of x axis">
                    <Expression>mod(cell.center.x,size.x)</Expression>
                </Rule>
                <Annotation>This modified x-coordinate is used for the wrap-around domain, so that particles that leave the domain edge are given a coordinate modulo the domain length. This method is used to insure that the trails (a.k.a. actin flaments) of all the particles are shown on the same periodic domain.</Annotation>
            </System>
        </CellType>
    </CellTypes>
    <CPM>
        <Interaction default="0.0">
            <Contact type1="BarbedEnd" type2="BarbedEnd" value="40"/>
            <Contact type1="BarbedEnd" type2="medium" value="20"/>
        </Interaction>
        <MonteCarloSampler stepper="edgelist">
            <MCSDuration value="1"/>
            <Neighborhood>
                <Order>2</Order>
            </Neighborhood>
            <MetropolisKinetics temperature="2"/>
        </MonteCarloSampler>
        <ShapeSurface scaling="norm">
            <Neighborhood>
                <Order>6</Order>
            </Neighborhood>
        </ShapeSurface>
    </CPM>
    <CellPopulations>
        <Population type="BarbedEnd" size="1">
            <InitRectangle random-offset="5" number-of-cells="5" mode="random">
                <Dimensions origin="0.0, 0.0, 0.0" size="200.0, 20.0, 0.0"/>
            </InitRectangle>
            <Annotation>Initate all the barbed ends close to the lower part of the domain, so we can watch them move up.</Annotation>
        </Population>
    </CellPopulations>
    <Analysis>
        <Gnuplotter time-step="20" decorate="true">
            <Terminal name="png"/>
            <Annotation>Plot the actual locations of the barbed ends and assign them each the "branch order" (i.e. number of divisions) that they belong to.</Annotation>
            <Plot>
                <Cells flooding="true" min="0" value="nd">
                    <ColorMap>
                        <Color value="0" color="yellow"/>
                        <Color value="5" color="red"/>
                        <Color value="8" color="blue"/>
                        <Color value="12" color="black"/>
                    </ColorMap>
                </Cells>
                <CellLabels value="nd"/>
            </Plot>
        </Gnuplotter>
        <Logger time-step="10">
            <Annotation>Draw the "trails" left by the particles, i.e. the branched structure of the actin filaments.</Annotation>
            <Input>
                <Symbol symbol-ref="cell.center.x"/>
                <Symbol symbol-ref="cell.center.y"/>
            </Input>
            <Output>
                <TextOutput/>
            </Output>
            <Plots>
                <Plot time-step="-1">
                    <Style point-size="1" style="points"/>
                    <Terminal terminal="png"/>
                    <X-axis maximum="200.0" minimum="0.0">
                        <Symbol symbol-ref="xc"/>
                    </X-axis>
                    <Y-axis maximum="200.0" minimum="0.0">
                        <Symbol symbol-ref="cell.center.y"/>
                    </Y-axis>
                    <Color-bar reverse-palette="true">
                        <Symbol symbol-ref="time"/>
                    </Color-bar>
                </Plot>
            </Plots>
        </Logger>
        <ModelGraph format="svg" reduced="false" include-tags="#untagged"/>
    </Analysis>
</MorpheusModel>

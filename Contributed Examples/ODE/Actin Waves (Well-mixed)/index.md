---
MorpheusModelID: M2012

authors: [L. Edelstein-Keshet]
contributors: [Y. Xiao]

title: Well-mixed Actin Waves

# Under review
hidden: false
private: false

# Reference details
#publication:
#  doi: ""
#  title: ""
#  journal: ""
#  volume:
#  issue:
#  page: ""
#  year:
#  original_model: true
#  preprint: false

tags:
- Actin
- Actin Wave
- Bistable System
- GTPase
- F-actin
- Filament
- Microfilament
- Negative Feedback
- ODE
- Ordinary Differential Equation
- Oscillation
- Wave
- Well-mixed

categories:
- "L. Edelstein-Keshet: Mathematical Models in Cell Biology"
---
> ODEs for GTPase interacting with F-actin

## Introduction

Here, we explore the time behavior of a well-mixed GTPase model with negative feedback from F-actin to the GTPase.

## Description

We assume that GTPase (<span title="//Global/Variable[@symbol='a']">$`G`$</span>) leads to actin assembly, and that F-actin, denoted by the variable (<span title="//Global/Variable[@symbol='F_a']">$`F`$</span>), leads to GTPase inactivation:

$$\begin{align}
\frac{\mathrm dG}{\mathrm dt} &= (b + \gamma \frac{G^n}{1 + G^n})G_i - G(\eta + sF) \\\\
\frac{\mathrm dF}{\mathrm dt} &= \epsilon(k_nG - k_sF) \\\\
\end{align}$$

## Results

The model illustrates that cycling can ocurr in a bistable system with some negative feedback as shown in the figure below.

![](ActinWavesMorpheusWM.png "In the well-mixed single GTPase model, when actin negative feedback is included, the system can begin to oscillate. In the Morpheus model, <span title='//Global/Variable[@symbol=&#39;a&#39;]'>$`a`$</span> represents active GTPase (denoted by <span title='//Global/Variable[@symbol=&#39;a&#39;]'>$`G`$</span> in the [ODEs above](#description)) and <span title='//Global/Variable[@symbol=&#39;F_a&#39;]'>$`F_a`$</span> (denoted by <span title='//Global/Variable[@symbol=&#39;F_a&#39;]'>$`F`$</span> [above](#description)) is the filamentous actin.")

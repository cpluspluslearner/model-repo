<MorpheusModel version="4">
    <Description>
        <Title>FN (PDE) in 2D</Title>
        <Details>Full title:		FitzHugh–Nagumo Waves
Authors:		L. Edelstein-Keshet
Contributors:	Y. Xiao
Date:		23.06.2022
Software:		Morpheus (open-source). Download from https://morpheus.gitlab.io
Model ID:		https://identifiers.org/morpheus/M2014
File type:		Supplementary model
Reference:		L. Edelstein-Keshet: Mathematical Models in Cell Biology
Comment:		PDE model for FitzHugh–Nagumo equations solved in 2D. There is current injected at the "origin" to kickstart the waves. The initial profiles of u and v are asymmetric to provoke spirals. This simulation produces spiral waves.</Details>
    </Description>
    <!--
 Define the variables and initialize their values.
 Indicate the rates of diffusion
 GTPases diffusem but F-actin does not
-->
    <Global>
        <Field symbol="u" name="voltage" value="2.5*(0.5-space.y/size.y)">
            <Diffusion rate="0.00005"/>
        </Field>
        <Field symbol="v" name="refractory variable" value="space.x/size.x">
            <!--    <Disabled>
        <Diffusion rate="0.00001"/>
    </Disabled>
-->
        </Field>
        <!--
 Specify the method of solution and give the kinetic terms
 in the differential equations for the three variables
-->
        <System time-step="0.05" solver="Runge-Kutta [fixed, O(4)]">
            <DiffEqn symbol-ref="u">
                <Expression> u*(1-u)*(u-a)-v + I_0 </Expression>
            </DiffEqn>
            <DiffEqn symbol-ref="v">
                <Expression>epsilon*(u-b*v) </Expression>
            </DiffEqn>
            <!--
  Define the constants and give their values.
-->
            <Constant symbol="b" name="decay rate" value="1.0"/>
            <Constant symbol="a" name="middle value" value="0.25"/>
            <!--    <Disabled>
        <Constant symbol="I_0" name="input current" value="0.5"/>
    </Disabled>
-->
            <Constant symbol="epsilon" name="actin reaction rate" value="0.002"/>
            <Function symbol="I_0">
                <Expression>0.2+0.6*exp(-(((space.x)/size.x)^2+((space.y)/size.y)^2)/0.01) </Expression>
            </Function>
        </System>
        <!--    <Disabled>
        <Variable symbol="I_0" value="(space.x/size.x-0.5)^2"/>
    </Disabled>
-->
    </Global>
    <!--
Specify the 2D Domain ("Square" lattice) and its boundary conditions
Indicate the size of each spatial box
-->
    <Space>
        <Lattice class="square">
            <Size symbol="size" value="100, 100, 0"/>
            <BoundaryConditions>
                <Condition type="periodic" boundary="x"/>
                <Condition type="periodic" boundary="y"/>
            </BoundaryConditions>
            <NodeLength value="0.02"/>
            <Neighborhood>
                <Order>1</Order>
            </Neighborhood>
        </Lattice>
        <SpaceSymbol symbol="space"/>
    </Space>
    <!--
  Set the start and end time and any random seed needed
-->
    <Time>
        <StartTime value="0"/>
        <StopTime value="1500"/>
        <SaveInterval value="0"/>
        <RandomSeed value="3451"/>
        <TimeSymbol symbol="time"/>
    </Time>
    <!--
 Specify how the results should be plotted
-->
    <Analysis>
        <Gnuplotter time-step="20" decorate="true">
            <Terminal name="png"/>
            <!--
Side by side plots of the active GTPase and the F-Actin
A color scheme an be specified  (e.g. for F-actin)
Both plots are on the same "terminal"
-->
            <Plot>
                <Field symbol-ref="u" min="0"/>
            </Plot>
            <Plot>
                <Field symbol-ref="v">
                    <ColorMap>
                        <Color value="12.0" color="blue"/>
                        <Color value="10.0" color="green"/>
                        <Color value="8.0" color="yellow"/>
                        <Color value="6.0" color="white"/>
                    </ColorMap>
                </Field>
            </Plot>
        </Gnuplotter>
        <Logger time-step="100">
            <Input>
                <Symbol symbol-ref="u"/>
            </Input>
            <Output>
                <TextOutput file-format="csv"/>
            </Output>
            <Plots>
                <!--
Make a line plot showing the profiles of all three variables
    -->
                <Plot time-step="200">
                    <Style line-width="3.0" style="lines"/>
                    <Terminal terminal="png"/>
                    <X-axis>
                        <Symbol symbol-ref="space.x"/>
                    </X-axis>
                    <Y-axis>
                        <Symbol symbol-ref="u"/>
                        <Symbol symbol-ref="v"/>
                    </Y-axis>
                    <!--
   Creates that plot once at the end of the simulation
-->
                    <Range>
                        <Data/>
                        <Time mode="current"/>
                    </Range>
                </Plot>
            </Plots>
            <Restriction>
                <Slice axis="y" value="size.y/2"/>
            </Restriction>
            <!--
Indicates that the line plot is for a slice midway up the y axis
-->
        </Logger>
        <!--
Make a kimograph of the variable a
that shows how a 1D slice midway up the y axis
develops over time.
-->
        <Logger time-step="10">
            <Input>
                <Symbol symbol-ref="u"/>
            </Input>
            <Plots>
                <SurfacePlot time-step="150">
                    <Color-bar>
                        <Symbol symbol-ref="u"/>
                    </Color-bar>
                    <Terminal terminal="png"/>
                </SurfacePlot>
            </Plots>
            <Restriction>
                <Slice axis="y" value="size.y/2"/>
            </Restriction>
            <Output>
                <TextOutput/>
            </Output>
        </Logger>
        <ModelGraph format="dot" reduced="false" include-tags="#untagged"/>
    </Analysis>
</MorpheusModel>
